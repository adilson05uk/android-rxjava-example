package com.thinkincode.rxjavaexample.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import com.thinkincode.rxjavaexample.R
import com.thinkincode.rxjavaexample.eventbus.EventBusActivity
import com.thinkincode.rxjavaexample.job.JobExecutorActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onShowJobExecutorActivityButtonClicked(view: View) {
        startActivity(Intent(this, JobExecutorActivity::class.java))
    }

    fun onShowEventBusActivityButtonClicked(view: View) {
        startActivity(Intent(this, EventBusActivity::class.java))
    }
}

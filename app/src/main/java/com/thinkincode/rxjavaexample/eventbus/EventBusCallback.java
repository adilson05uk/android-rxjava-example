package com.thinkincode.rxjavaexample.eventbus;

public interface EventBusCallback<T> {

    void onEvent(T event);
}

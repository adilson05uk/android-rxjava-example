package com.thinkincode.rxjavaexample.eventbus

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject

/**
 * Posts events on the main thread.
 */
class EventBus<T> {

    private val subject: PublishSubject<T> = PublishSubject.create()

    fun publishEvent(o: T) {
        subject.onNext(o)
    }

    fun subscribe(callback: EventBusCallback<T>): EventBusSubscription {
        val consumer = Consumer<T> { callback.onEvent(it) }

        val disposable = subject.observeOn(AndroidSchedulers.mainThread()).subscribe(consumer)

        return EventBusSubscription(disposable)
    }

}
package com.thinkincode.rxjavaexample.eventbus

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.thinkincode.rxjavaexample.R
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_event_bus.*
import java.util.concurrent.atomic.AtomicInteger

class EventBusActivity : AppCompatActivity() {

    private val eventBus: EventBus<String> = EventBus()
    private val subscriptions: MutableList<EventBusSubscription> = mutableListOf()
    private val receivedEventsStringBuilder: StringBuilder = StringBuilder()
    private var publishedEventsCount: AtomicInteger = AtomicInteger(0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_event_bus)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        updateSubscriptionsCountTextView()
    }

    override fun onDestroy() {
        super.onDestroy()
        cancelAllSubscriptions()
    }

    fun onCreateSubscriptionButtonClicked(view: View) {
        val disposable = eventBus.subscribe(EventBusCallback { event -> onEventReceived(event) })
        subscriptions.add(disposable)
        updateSubscriptionsCountTextView()
    }

    fun onCancelAllSubscriptionsButtonClicked(view: View) {
        cancelAllSubscriptions()
    }

    fun onPublishSingleEventFromMainThreadButtonClicked(view: View) {
        publishEvent()
    }

    fun onPublishSingleEventFromBackgroundThreadButtonClicked(view: View) {
        Single.create<Any>({ publishEvent() })
            .subscribeOn(Schedulers.newThread())
            .subscribe()
    }

    fun onPublishMultipleEventsFromMultipleBackgroundThreadsButtonClicked(view: View) {
        val single = Single.create<Any>({ publishEvent() })
            .subscribeOn(Schedulers.newThread())

        repeat(NUMBER_OF_BACKGROUND_THREADS) { single.subscribe() }
    }

    private fun cancelAllSubscriptions() {
        subscriptions.forEach { it.cancel() }
        subscriptions.clear()
        updateSubscriptionsCountTextView()
    }

    private fun publishEvent() {
        val event = "Event${publishedEventsCount.incrementAndGet()}"

        Log.d(this.javaClass.name, "Sending $event on thread ${Thread.currentThread().name}")
        eventBus.publishEvent(event)
        Log.d(this.javaClass.name, "Sent $event on thread ${Thread.currentThread().name}")
    }

    private fun onEventReceived(event: Any) {
        val message = "Received $event on thread ${Thread.currentThread().name}"

        Log.d(this.javaClass.name, message)

        receivedEventsStringBuilder.append(message)
        receivedEventsStringBuilder.appendln()

        receivedEventsTextView.text = receivedEventsStringBuilder.toString()
    }

    private fun updateSubscriptionsCountTextView() {
        subscriptionsCountTextView.text = "${subscriptions.size} active subscriptions"
    }

    companion object {
        const val NUMBER_OF_BACKGROUND_THREADS: Int = 10
    }
}

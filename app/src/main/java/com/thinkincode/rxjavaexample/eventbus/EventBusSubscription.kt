package com.thinkincode.rxjavaexample.eventbus

import io.reactivex.disposables.Disposable

class EventBusSubscription(private val disposable: Disposable) {

    fun cancel() {
        disposable.dispose()
    }
}
package com.thinkincode.rxjavaexample.job

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Posts [Job] instances to be executed on a background thread.
 */
class JobExecutor {

    private val jobObserversRegistry: JobObserversRegistry = JobObserversRegistry()

    /**
     * Stops all jobs that are either executing or awaiting execution.
     */
    fun stopAll() {
        jobObserversRegistry.clear()
    }

    /**
     * Posts the given [Job] instance to be executed on a background thread
     * and passes the result of the [Job] execution to the given [JobListener] on the main thread.
     *
     * @param job the [Job] instance to execute.
     * @param jobListener the [JobListener] to report success or error to.
     * @param T the type of result returned on successful execution of the [Job].
     */
    fun <T> execute(
        job: Job<T>,
        jobListener: JobListener<T>
    ) {
        Single.create(JobOnSubscribe(job))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(JobObserver(jobListener, jobObserversRegistry))
    }

    /**
     * Posts the given [Job] instance to be executed on a background thread
     * and ignores the result of the [Job] execution.
     *
     * @param job the [Job] instance to execute.
     * @param T the type of result returned on successful execution of the [Job].
     */
    fun <T> execute(job: Job<T>) {
        Single.create(JobOnSubscribe(job))
            .subscribeOn(Schedulers.io())
            .subscribe()
    }
}

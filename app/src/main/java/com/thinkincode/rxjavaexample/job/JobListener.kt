package com.thinkincode.rxjavaexample.job

/**
 * Listener that receives the result of an asynchronous job execution.
 *
 * @param T the type of result returned on successful execution of the job.
 */
interface JobListener<in T> {

    /**
     * Callback for the when the job executes successfully.
     *
     * @param success the result of the job execution.
     */
    fun onSuccess(success: T)

    /**
     * Callback for when the job fails execution.
     *
     * @param error the cause of the error.
     */
    fun onError(error: Throwable)
}

package com.thinkincode.rxjavaexample.job

import io.reactivex.disposables.Disposable

/**
 * Registry for keeping track of which [JobObserver] is associated to which [Disposable].
 */
internal class JobObserversRegistry {

    private val subscriptions: MutableMap<JobObserver<*>, Disposable> = mutableMapOf()

    /**
     * Creates a subscription record.
     */
    @Synchronized
    fun register(
        jobObserver: JobObserver<*>,
        disposable: Disposable
    ) {
        subscriptions[jobObserver] = disposable
    }

    /**
     * Removes a subscription record.
     */
    @Synchronized
    fun unregister(jobObserver: JobObserver<*>) {
        subscriptions.remove(jobObserver)
    }

    /**
     * Cancels and clears all subscriptions.
     */
    @Synchronized
    fun clear() {
        subscriptions.values.forEach { it.dispose() }
        subscriptions.clear()
    }
}

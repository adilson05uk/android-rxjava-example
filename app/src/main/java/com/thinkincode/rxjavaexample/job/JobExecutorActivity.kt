package com.thinkincode.rxjavaexample.job

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import com.thinkincode.rxjavaexample.R
import kotlinx.android.synthetic.main.activity_job_executor.*

class JobExecutorActivity : AppCompatActivity() {

    private val jobExecutor: JobExecutor = JobExecutor()
    private val outputStringBuilder: StringBuilder = StringBuilder()
    private var count: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job_executor)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopBackgroundJobs()
    }

    fun onStartMultipleSuccessJobsButtonClicked(view: View) {
        stateTextView.text = "Starting execution of JobA..."

        val jobListener: JobListener<String> = object : JobListener<String> {

            override fun onSuccess(success: String) {
                this@JobExecutorActivity.onJobSuccess(success)
            }

            override fun onError(error: Throwable) {
                this@JobExecutorActivity.onJobError(error)
            }
        }

        repeat(NUMBER_OF_BACKGROUND_THREADS) { jobExecutor.execute(JobA(++count), jobListener) }
    }

    fun onStartSingleSuccessJobButtonClicked(view: View) {
        stateTextView.text = "Starting execution of JobA..."

        val jobListener: JobListener<String> = object : JobListener<String> {

            override fun onSuccess(success: String) {
                this@JobExecutorActivity.onJobSuccess(success)
            }

            override fun onError(error: Throwable) {
                this@JobExecutorActivity.onJobError(error)
            }
        }

        jobExecutor.execute(JobA(++count), jobListener)
    }

    fun onStartSingleFailureJobButtonClicked(view: View) {
        stateTextView.text = "Starting execution of JobB..."

        val jobListener: JobListener<String> = object : JobListener<String> {

            override fun onSuccess(success: String) {
                this@JobExecutorActivity.onJobSuccess(success)
            }

            override fun onError(error: Throwable) {
                this@JobExecutorActivity.onJobError(error)
            }
        }

        jobExecutor.execute(JobB(), jobListener)
    }

    fun onStopBackgroundJobsButtonClicked(view: View) {
        stopBackgroundJobs()
    }

    internal fun onJobSuccess(success: String) {
        outputStringBuilder.append(success)
        outputStringBuilder.appendln()

        outputTextView.text = outputStringBuilder.toString()
        stateTextView.text = "Background job completed!"
    }

    internal fun onJobError(error: Throwable) {
        stateTextView.text = "Background job error'ed: " + error.message
    }

    private fun stopBackgroundJobs() {
        jobExecutor.stopAll()
        stateTextView.text = "Stopped all background jobs."
    }

    companion object {
        const val NUMBER_OF_BACKGROUND_THREADS: Int = 10
    }
}

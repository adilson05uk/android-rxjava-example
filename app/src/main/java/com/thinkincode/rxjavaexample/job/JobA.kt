package com.thinkincode.rxjavaexample.job

import android.util.Log

/**
 * Implementation of [Job] which returns a [String].
 */
class JobA(private val input: Int) : Job<String> {

    override fun execute(): String {
        Log.d("JobA", "Executing JobA on thread ${Thread.currentThread().name}")
        return "JobA -> " + input.toString()
    }
}

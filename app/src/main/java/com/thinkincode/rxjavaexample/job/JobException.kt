package com.thinkincode.rxjavaexample.job

/**
 * Thrown when an unexpected problem is encountered during execution of a [Job].
 */
class JobException(message: String) : Exception(message)
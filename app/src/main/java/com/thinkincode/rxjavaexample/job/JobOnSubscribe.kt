package com.thinkincode.rxjavaexample.job

import io.reactivex.SingleEmitter
import io.reactivex.SingleOnSubscribe

/**
 * Implementation of [SingleOnSubscribe] that executes a given [Job] instance
 * and reports success or error to the provided [SingleEmitter] instance.
 *
 * @param T the type of result returned on successful execution of the [Job].
 */
internal class JobOnSubscribe<T>(private val job: Job<T>) : SingleOnSubscribe<T> {

    override fun subscribe(singleEmitter: SingleEmitter<T>) {
        try {
            val output = job.execute()
            singleEmitter.onSuccess(output)
        } catch (e: JobException) {
            if (!singleEmitter.isDisposed) {
                singleEmitter.onError(e)
            }
        }
    }

}

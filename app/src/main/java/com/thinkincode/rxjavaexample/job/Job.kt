package com.thinkincode.rxjavaexample.job

/**
 * Represents a job that is to be scheduled and executed in a background thread.
 *
 * @param <T> the return type of the job.
 */
interface Job<out T> {

    /**
     * Executes the job.
     *
     * @return the result of the job execution.
     * @throws JobException in case of error.
     */
    @Throws(JobException::class)
    fun execute(): T
}

package com.thinkincode.rxjavaexample.job

import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

/**
 * Implementation of [SingleObserver] which keeps a handle on the [Disposable] instance
 * that is created when a subscription is established.
 *
 * @param T the type of result returned on successful execution of the job.
 */
internal class JobObserver<T>(
    private val jobListener: JobListener<T>,
    private val jobObserversRegistry: JobObserversRegistry
) : SingleObserver<T> {

    override fun onSubscribe(disposable: Disposable) {
        jobObserversRegistry.register(this, disposable)
    }

    override fun onSuccess(success: T) {
        jobObserversRegistry.unregister(this)
        jobListener.onSuccess(success)
    }

    override fun onError(error: Throwable) {
        jobObserversRegistry.unregister(this)
        jobListener.onError(error)
    }
}

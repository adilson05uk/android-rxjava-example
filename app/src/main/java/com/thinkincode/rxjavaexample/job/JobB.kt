package com.thinkincode.rxjavaexample.job

import android.util.Log

/**
 * Implementation of [Job] which throws a [JobException].
 */
class JobB : Job<String> {

    @Throws(JobException::class)
    override fun execute(): String {
        Log.d("JobB", "Executing JobB on thread ${Thread.currentThread().name}")
        throw JobException("Failed execution in JobB!")
    }
}

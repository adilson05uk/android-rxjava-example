package com.thinkincode.rxjavaexample;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@RunWith(JUnit4.class)
public class RxObservableUnitTest {

    private List<Integer> outputItems;

    private Disposable disposable;

    @Before
    public void setUp() {
        outputItems = new ArrayList<>();
    }

    @Test
    public void test_observable_map() {
        // Given.
        Observable<Integer> observable = Observable.just(1, 2, 2, 3)
                .map(i -> i * 100)
                .filter(i -> i > 100)
                .distinct()
                .doOnNext(nextItem -> outputItems.add(nextItem));

        // When.
        disposable = observable.subscribe();

        // Then.
        assertEquals(Arrays.asList(200, 300), outputItems);

        // And.
        assertTrue(disposable.isDisposed());
    }

    @Test
    public void test_observable_flatMap() {
        // Given.
        Observable<Integer> observable = Observable.just("1/2/3", "4/5/6", "7/8/9")
                .flatMap(i -> Observable.fromArray(i.split("/")))
                .map(Integer::valueOf)
                .doOnNext(nextItem -> outputItems.add(nextItem));

        // When.
        disposable = observable.subscribe();

        // Then.
        assertEquals(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9), outputItems);

        // And.
        assertTrue(disposable.isDisposed());
    }
}

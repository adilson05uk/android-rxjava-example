package com.thinkincode.rxjavaexample;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@RunWith(JUnit4.class)
public class RxSingleUnitTest {

    private Integer outputItem;

    @Before
    public void setUp() {
        outputItem = null;
    }

    @Test
    public void test_single_map() {
        // Given.
        Single<Integer> single = Single.just(1)
                .map(i -> i * 100)
                .doOnSuccess(success -> outputItem = success);

        // When.
        Disposable disposable = single.subscribe();

        // Then.
        assertEquals(Integer.valueOf(100), outputItem);

        // And.
        assertTrue(disposable.isDisposed());
    }
}

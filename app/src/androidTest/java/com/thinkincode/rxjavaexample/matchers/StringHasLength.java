package com.thinkincode.rxjavaexample.matchers;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class StringHasLength extends TypeSafeMatcher<String> {

    private final int length;

    public static Matcher<String> hasLength(int length) {
        return new StringHasLength(length);
    }

    private StringHasLength(int length) {
        this.length = length;
    }

    @Override
    public boolean matchesSafely(String item) {
        return item.length() == length;
    }

    @Override
    public void describeMismatchSafely(String item, Description mismatchDescription) {
        mismatchDescription.appendValue(item).appendText(" does not have length ").appendValue(length);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("a string with length ").appendValue(length);
    }

}
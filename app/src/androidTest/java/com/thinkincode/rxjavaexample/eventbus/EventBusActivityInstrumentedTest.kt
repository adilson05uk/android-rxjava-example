package com.thinkincode.rxjavaexample.eventbus

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.thinkincode.rxjavaexample.R
import com.thinkincode.rxjavaexample.matchers.StringHasLength.hasLength
import org.hamcrest.Matchers.containsString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EventBusActivityInstrumentedTest {

    @JvmField @Rule var activityTestRule = ActivityTestRule(EventBusActivity::class.java)

    @Test
    fun test_publish_one_event_from_main_thread_with_one_active_subscription() {
        // Given.
        onView(withId(R.id.createSubscriptionButton)).perform(click())

        // When.
        onView(withId(R.id.publishSingleEventFromMainThreadButton)).perform(click())

        // Then.
        onView(withId(R.id.receivedEventsTextView))
            .check(matches(withText("Received Event1 on thread main\n")));
    }

    @Test
    fun test_publish_one_event_from_background_thread_with_one_active_subscription() {
        // Given.
        onView(withId(R.id.createSubscriptionButton)).perform(click())

        // When.
        onView(withId(R.id.publishSingleEventFromBackgroundThreadButton)).perform(click())

        // Then.
        onView(withId(R.id.receivedEventsTextView))
            .check(matches(withText("Received Event1 on thread main\n")));
    }

    @Test
    fun test_publish_one_event_from_main_thread_with_two_active_subscriptions() {
        // Given.
        onView(withId(R.id.createSubscriptionButton))
            .perform(click())
            .perform(click())

        // When.
        onView(withId(R.id.publishSingleEventFromMainThreadButton)).perform(click())

        // Then.
        onView(withId(R.id.receivedEventsTextView))
            .check(matches(withText("Received Event1 on thread main\nReceived Event1 on thread main\n")));
    }

    @Test
    fun test_publish_one_event_from_background_thread_with_two_active_subscriptions() {
        // Given.
        onView(withId(R.id.createSubscriptionButton))
            .perform(click())
            .perform(click())

        // When.
        onView(withId(R.id.publishSingleEventFromBackgroundThreadButton)).perform(click())

        // Then.
        onView(withId(R.id.receivedEventsTextView))
            .check(matches(withText("Received Event1 on thread main\nReceived Event1 on thread main\n")));
    }

    @Test
    fun test_publish_two_events_from_background_thread_with_one_active_subscription() {
        // Given.
        onView(withId(R.id.createSubscriptionButton)).perform(click())

        // When.
        onView(withId(R.id.publishSingleEventFromBackgroundThreadButton))
            .perform(click())
            .perform(click())

        // Then.
        onView(withId(R.id.receivedEventsTextView))
            .check(matches(withText("Received Event1 on thread main\nReceived Event2 on thread main\n")));
    }

    @Test
    fun test_publish_multiple_events_from_multiple_background_threads_with_one_active_subscription() {
        // Given.
        onView(withId(R.id.createSubscriptionButton)).perform(click())

        // When.
        onView(withId(R.id.publishMultipleEventsFromBackgroundThreadsButton)).perform(click())

        // Then.
        val viewInteraction = onView(withId(R.id.receivedEventsTextView))

        var expectedLength = 0;

        repeat(EventBusActivity.NUMBER_OF_BACKGROUND_THREADS) {
            val text = "Received Event${it + 1} on thread main\n"
            viewInteraction.check(matches(withText(containsString(text))))
            expectedLength += text.length
        }

        viewInteraction.check(matches(withText(hasLength(expectedLength))))
    }
}
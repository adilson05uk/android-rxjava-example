package com.thinkincode.rxjavaexample.job

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.thinkincode.rxjavaexample.R
import com.thinkincode.rxjavaexample.matchers.StringHasLength.hasLength
import org.hamcrest.CoreMatchers.containsString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class JobExecutorActivityInstrumentedTest {

    @JvmField @Rule var activityTestRule = ActivityTestRule(JobExecutorActivity::class.java)

    @Test
    fun test_schedule_multiple_jobs_which_succeed() {
        // When.
        onView(withId(R.id.startMultipleSuccessJobsButton)).perform(click())

        // Then.
        val viewInteraction = onView(withId(R.id.outputTextView))

        var expectedLength = 0;

        repeat(JobExecutorActivity.NUMBER_OF_BACKGROUND_THREADS) {
            val text = "JobA -> ${it + 1}\n"
            viewInteraction.check(matches(withText(containsString(text))))
            expectedLength += text.length
        }

        viewInteraction.check(matches(withText(hasLength(expectedLength))))
    }

    @Test
    fun test_schedule_one_job_which_succeeds() {
        // When.
        onView(withId(R.id.startSingleSuccessJobButton)).perform(click())

        // Then.
        onView(withId(R.id.stateTextView)).check(matches(withText("Background job completed!")))

        // And.
        onView(withId(R.id.outputTextView)).check(matches(withText("JobA -> 1\n")));
    }

    @Test
    fun test_schedule_one_job_which_fails() {
        // When.
        onView(withId(R.id.startSingleFailureJobButton)).perform(click())

        // Then.
        onView(withId(R.id.stateTextView)).check(matches(withText("Background job error'ed: Failed execution in JobB!")))
    }
}
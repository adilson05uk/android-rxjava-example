# RxJava Android Application Example

This project demonstrates how to build an [EventBus](app/src/main/java/com/thinkincode/rxjavaexample/eventbus/EventBus.kt) and [JobExecutor](app/src/main/java/com/thinkincode/rxjavaexample/job/JobExecutor.kt) with RxJava classes but in such a way that the RxJava classes are abstracted away and not leaked out of the [EventBus](app/src/main/java/com/thinkincode/rxjavaexample/eventbus/EventBus.kt) and [JobExecutor](app/src/main/java/com/thinkincode/rxjavaexample/job/JobExecutor.kt) public interfaces.

Refer to the application's [build.gradle](app/build.gradle) file for the required dependencies.

Launch the Android application to see the [EventBus](app/src/main/java/com/thinkincode/rxjavaexample/eventbus/EventBus.kt) and [JobExecutor](app/src/main/java/com/thinkincode/rxjavaexample/job/JobExecutor.kt) classes in action!
